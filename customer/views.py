from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.core.context_processors import csrf
from django.contrib.auth.models import auth, User
from customer.models import Profile, ProfileForm
from django.db.models import Q
from django.contrib.auth.decorators import login_required

def login(request):
    """Login form."""

    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)

        if user is not None and user.is_active:
            auth.login(request, user)
            try:
                resirect_url = request.META.get('HTTP_REFERER').split('?next=')[1]
            except IndexError:
                resirect_url = '/'
            return redirect(resirect_url)
        else:
            c = dict(username=username, error_message=_('Wrong password or User name'))
            c.update(csrf(request))
            return render_to_response("loginform.html", c)
    else:
        c = {}
        c.update(csrf(request))

        return render_to_response("loginform.html", c)

def logout(request):
    """Logout."""

    auth.logout(request)
    return redirect('/login')
